var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Task = require('./api/models/todoListModel'),
  bodyParser = require('body-parser');
  
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Tododb'); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/todoListRoutes');
routes(app);


app.listen(port);


console.log('todo list RESTful API server started on: ' + port);

// Get all Tasks  ( GET )
// http://localhost:3000/tasks/  

// Create a Task  ( POST )
// http://localhost:3000/tasks   

// Update a task  ( PUT )
// http://localhost:3000/tasks/taskID

// Delete a task ( DELETE )
// http://localhost:3000/tasks/taskID